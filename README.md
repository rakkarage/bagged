# Bagged

## Todo

- <https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new?tabs=netcore21>
- <https://docs.microsoft.com/en-us/aspnet/core/tutorials/build-your-first-blazor-app?view=aspnetcore-3.0>
- <https://visualstudiomagazine.com/articles/2019/02/01/navigating-in-blazor.aspx>
- <https://exceptionnotfound.net/using-blazor-to-build-a-client-side-single-page-app-with-net-core/>

### Authentication

- <https://docs.microsoft.com/en-ca/aspnet/core/signalr/authn-and-authz?view=aspnetcore-3.0>
- <https://www.talkingdotnet.com/how-to-scaffold-identity-ui-in-asp-net-core-2-1/>

### Database

- <https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/new-db?tabs=netcore-cli>
- <https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/?view=aspnetcore-2.2>
- <https://www.learnrazorpages.com/miscellaneous/scaffolding>
- <https://code.msdn.microsoft.com/ASPNET-Core-Blazor-CRUD-b4085178>
- <https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/adding-controller?view=aspnetcore-3.0&tabs=visual-studio>
- <https://learn-blazor.com/architecture/rest-api/>

### Forms & Validation

- <https://docs.microsoft.com/en-us/aspnet/core/blazor/forms-validation?view=aspnetcore-3.0>
- <https://www.c-sharpcorner.com/article/validate-your-blazor-form-using-the-editform/>
