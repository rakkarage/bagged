using System;
using System.Collections.Generic;
namespace Bagged.Data
{
	public class Channel
	{
		public int ChannelId { get; set; }
		public DateTime Date { get; set; }
		public string Name { get; set; }

		public List<Message> Messages { get; set; }

		public int UserId { get; set; }
		public User User { get; set; }
	}
}