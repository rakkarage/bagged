using Microsoft.EntityFrameworkCore;
namespace Bagged.Data
{
	public class BaggedContext : DbContext
	{
		public DbSet<User> Users { get; set; }
		public DbSet<Message> Messages { get; set; }
		public DbSet<Channel> Channels { get; set; }
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite("Data Source=Bagged.db");
		}
	}
}
