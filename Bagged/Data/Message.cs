using System;
using System.Collections.Generic;
namespace Bagged.Data
{
	public class Message
	{
		public int MessageId { get; set; }
		public DateTime Date { get; set; }
		public string Text { get; set; }

		public string ChannelId { get; set; }
		public Channel Channel { get; set; }

		public int UserId { get; set; }
		public User User { get; set; }
	}
}