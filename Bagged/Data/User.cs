using ca.HenrySoftware;
using System;
using System.Collections.Generic;
namespace Bagged.Data
{
	public class User
	{
		public int UserId { get; set; }
		public DateTime Date { get; set; }
		public string Name { get; set; }

		public List<Message> Messages { get; set; }
	}
}
