﻿// <auto-generated />
using System;
using Bagged.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Bagged.Migrations
{
    [DbContext(typeof(BaggedContext))]
    partial class ChatContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0-preview4.19216.3");

            modelBuilder.Entity("Bagged.Data.Channel", b =>
                {
                    b.Property<int>("ChannelId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Name");

                    b.Property<int>("UserId");

                    b.HasKey("ChannelId");

                    b.HasIndex("UserId");

                    b.ToTable("Channels");
                });

            modelBuilder.Entity("Bagged.Data.Message", b =>
                {
                    b.Property<int>("MessageId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ChannelId");

                    b.Property<int?>("ChannelId1");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Text");

                    b.Property<int>("UserId");

                    b.HasKey("MessageId");

                    b.HasIndex("ChannelId1");

                    b.HasIndex("UserId");

                    b.ToTable("Messages");
                });

            modelBuilder.Entity("Bagged.Data.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Name");

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Bagged.Data.Channel", b =>
                {
                    b.HasOne("Bagged.Data.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Bagged.Data.Message", b =>
                {
                    b.HasOne("Bagged.Data.Channel", "Channel")
                        .WithMany("Messages")
                        .HasForeignKey("ChannelId1");

                    b.HasOne("Bagged.Data.User", "User")
                        .WithMany("Messages")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
